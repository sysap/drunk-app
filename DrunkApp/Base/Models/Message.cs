﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Models
{
    public class Message
    {
        public EMessageType MessageType { get; set; }

        public string TextMessage { get; set; }
    }
}
