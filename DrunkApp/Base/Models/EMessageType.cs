﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Models
{
    public enum EMessageType
    {
        MessageBox = 1,
        Mause = 2,
        Clipboard = 3,
        Browser = 4,
        BeepMario = 5,
        SetDesktop = 6,
        IsAlive = 7,
    }
}
