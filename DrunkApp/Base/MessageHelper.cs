﻿using Base.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base
{
    public class MessageHelper : IDisposable
    {
        public Message DecodeMessage(string message)
        {
            try
            {
                return JsonConvert.DeserializeObject<Message>(message.Substring(0,message.Length -1));
            }
            catch(Exception e)
            {
                var k = e;
                return null;
            }
        }

        public string EncodeMessage(Message message)
        {
            return JsonConvert.SerializeObject(message);
        }

        public void Dispose()
        {
        }
    }
}
