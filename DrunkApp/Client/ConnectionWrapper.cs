﻿using Base;
using Base.Models;
using SimpleTCP;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    class ConnectionWrapper
    {
        Thread connectionThread;
        MessageHelper messageHelper = new MessageHelper();
        SimpleTcpClient client;
        Stopwatch stopWatch = new Stopwatch();

        long timeout = 2000;


        private bool isConnected = false;
        public bool IsConnected { get { return isConnected; } }


        public ConnectionWrapper(SimpleTcpClient client)
        {
            this.client = client;
            connectionThread = new Thread(ConnectionCheck);
            connectionThread.Start();
            client.DataReceived += DataReceived;
        }

        private void DataReceived(object sender, SimpleTCP.Message message)
        {
            var resp = message.MessageString;
            if (resp.ToUpper() == "OK")
            {
                stopWatch.Stop();
                if (stopWatch.ElapsedMilliseconds <= timeout)
                {
                    isConnected = true;
                }
                else
                {
                    isConnected = false;
                }
            }
            else
            {
                //emit event
            }
        }

        private void ConnectionCheck()
        {
            while (true)
            {
                testConnection();
                Thread.Sleep(5000);
            }            
        }

        private void testConnection()
        {
            if (stopWatch.IsRunning)
            {
                if (stopWatch.ElapsedMilliseconds > timeout)
                {
                    isConnected = false;
                    stopWatch.Stop();
                    stopWatch.Reset();
                }
                return;
            }

            stopWatch.Start();
            messageHelper.EncodeMessage(new Base.Models.Message()
            {
                MessageType = EMessageType.IsAlive,
            });
        }
    }
}
