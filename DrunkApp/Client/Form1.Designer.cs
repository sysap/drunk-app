﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MessageTypeComboBox = new System.Windows.Forms.ComboBox();
            this.sendCommandButton = new System.Windows.Forms.Button();
            this.messageTextBox = new System.Windows.Forms.TextBox();
            this.IpAddressTextBox = new System.Windows.Forms.TextBox();
            this.connectButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // MessageTypeComboBox
            // 
            this.MessageTypeComboBox.FormattingEnabled = true;
            this.MessageTypeComboBox.Location = new System.Drawing.Point(12, 48);
            this.MessageTypeComboBox.Name = "MessageTypeComboBox";
            this.MessageTypeComboBox.Size = new System.Drawing.Size(267, 21);
            this.MessageTypeComboBox.TabIndex = 0;
            // 
            // sendCommandButton
            // 
            this.sendCommandButton.Location = new System.Drawing.Point(12, 101);
            this.sendCommandButton.Name = "sendCommandButton";
            this.sendCommandButton.Size = new System.Drawing.Size(267, 23);
            this.sendCommandButton.TabIndex = 1;
            this.sendCommandButton.Text = "Send";
            this.sendCommandButton.UseVisualStyleBackColor = true;
            this.sendCommandButton.Click += new System.EventHandler(this.sendCommandButton_Click);
            // 
            // messageTextBox
            // 
            this.messageTextBox.Location = new System.Drawing.Point(12, 75);
            this.messageTextBox.Name = "messageTextBox";
            this.messageTextBox.Size = new System.Drawing.Size(267, 20);
            this.messageTextBox.TabIndex = 2;
            // 
            // IpAddressTextBox
            // 
            this.IpAddressTextBox.Location = new System.Drawing.Point(12, 12);
            this.IpAddressTextBox.Name = "IpAddressTextBox";
            this.IpAddressTextBox.Size = new System.Drawing.Size(186, 20);
            this.IpAddressTextBox.TabIndex = 3;
            this.IpAddressTextBox.Text = "127.0.0.1";
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(204, 10);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(75, 23);
            this.connectButton.TabIndex = 4;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(299, 134);
            this.Controls.Add(this.connectButton);
            this.Controls.Add(this.IpAddressTextBox);
            this.Controls.Add(this.messageTextBox);
            this.Controls.Add(this.sendCommandButton);
            this.Controls.Add(this.MessageTypeComboBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox MessageTypeComboBox;
        private System.Windows.Forms.Button sendCommandButton;
        private System.Windows.Forms.TextBox messageTextBox;
        private System.Windows.Forms.TextBox IpAddressTextBox;
        private System.Windows.Forms.Button connectButton;
    }
}

