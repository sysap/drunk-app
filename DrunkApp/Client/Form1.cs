﻿using Base;
using Base.Models;
using SimpleTCP;
using System;
using System.Threading;
using System.Windows.Forms;

namespace Client
{
    public partial class Form1 : Form
    {
        MessageHelper MessageHelper = new MessageHelper();
        SimpleTcpClient client;


        public Form1()
        {
            InitializeComponent();


            foreach (var item in Enum.GetValues(typeof(EMessageType)))
            {
                MessageTypeComboBox.Items.Add(item);
            }

        }

        private void sendCommandButton_Click(object sender, EventArgs e)
        {
            SendMessage(new Base.Models.Message()
            {
                MessageType = (EMessageType)MessageTypeComboBox.SelectedItem,
                TextMessage = messageTextBox.Text,
            });            
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            client = new SimpleTcpClient().Connect(IpAddressTextBox.Text, 8910);
        }



        private void SendMessage(Base.Models.Message message)
        {
            client.WriteLine(MessageHelper.EncodeMessage(message));
        }
    }
}
